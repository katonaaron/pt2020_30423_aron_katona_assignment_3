package com.katonaaron.order.presentation.report;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.katonaaron.order.util.EntityUtil;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static com.katonaaron.order.util.EntityUtil.getColumnName;

/**
 * Generates a report about an entity in a PDF format.
 *
 * @param <T> type of the entity.
 */
public class ReportGenerator<T> {
    /**
     * Type of the entity.
     */
    private final Class<T> type;
    /**
     * Columns of the entity.
     */
    private final List<Field> columns;
    /**
     * Name of the database table associated with the entity.
     */
    private final String tableName;
    /**
     * Order number of the generated reports.
     */
    private int reportId = 1;

    /**
     * Initializes the fields.
     *
     * @param type type of the entity about which the reports will be generated.
     */
    public ReportGenerator(Class<T> type) {
        this.type = type;
        columns = EntityUtil.getColumns(this.type);
        tableName = EntityUtil.getTableName(this.type);
    }

    /**
     * Generates a new report a list of entity instances.
     *
     * @param entities list of entity instances.
     */
    public void generateReport(List<T> entities) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("report-" + tableName + "-" + reportId++ + ".pdf"));

            document.open();

            document.add(new Paragraph(tableName));

            PdfPTable table = new PdfPTable(columns.size());
            addTableHeader(table, columns);
            entities.forEach(entity -> addRow(table, columns, entity));

            document.add(table);
            document.close();
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the id of the next report.
     *
     * @return id of the next report.
     */
    public int getReportId() {
        return reportId;
    }

    /**
     * Increments the id of the next report.
     */
    public void incrementReportId() {
        reportId++;
    }

    /**
     * Adds the columns of the entity to the table of the report.
     *
     * @param table   table of the report.
     * @param columns columns of the entity.
     */
    private void addTableHeader(PdfPTable table, List<Field> columns) {
        columns.stream()
                .map(EntityUtil::getColumnName)
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * Appends an entity instance into the table of the report.
     *
     * @param table   table of the report.
     * @param columns columns of the entity.
     * @param entity  entity instance.
     */
    private void addRow(PdfPTable table, List<Field> columns, T entity) {
        try {
            for (Field column : columns) {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getColumnName(column), type);
                Object value = propertyDescriptor.getReadMethod().invoke(entity);
                table.addCell(value.toString());
            }
        } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
