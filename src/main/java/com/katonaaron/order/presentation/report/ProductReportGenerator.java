package com.katonaaron.order.presentation.report;

import com.katonaaron.order.annotations.Column;
import com.katonaaron.order.annotations.Id;
import com.katonaaron.order.model.StockItem;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Report generator products with quantity greater than zero.
 */
public class ProductReportGenerator {
    /**
     * Object used for generating reports.
     */
    private final ReportGenerator<Product> reportGenerator;

    /**
     * Initializes the fields.
     */
    public ProductReportGenerator() {
        reportGenerator = new ReportGenerator<>(Product.class);
    }

    /**
     * Generates a new report about products and stock items. Associates each product to its stockItem,
     * and prints only the ones that are on stock.
     *
     * @param products   list of products.
     * @param stockItems list of stock items.
     */
    public void generateReport(List<com.katonaaron.order.model.Product> products, List<StockItem> stockItems) {
        Map<String, Integer> productToQuantity = stockItems.stream()
                .collect(Collectors.toMap(StockItem::getProductName, StockItem::getQuantity));

        final List<Product> productDTOList = products.stream()
                .map(product -> new Product(product.getName(), productToQuantity.get(product.getName()), product.getUnitPrice()))
                .filter(product -> product.getQuantity() > 0)
                .collect(Collectors.toList());

        reportGenerator.generateReport(productDTOList);
    }


    /**
     * Data transfer object which is a union between Product and StockItem.
     */
    private static class Product {
        @Id
        @Column
        private String name;
        @Column
        private Integer quantity;
        @Column
        private Double unitPrice;

        public Product(String name, Integer quantity, Double unitPrice) {
            this.name = name;
            this.quantity = quantity == null ? 0 : quantity;
            this.unitPrice = unitPrice;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(Double unitPrice) {
            this.unitPrice = unitPrice;
        }
    }
}
