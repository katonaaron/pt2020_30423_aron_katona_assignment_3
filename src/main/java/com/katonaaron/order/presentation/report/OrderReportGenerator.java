package com.katonaaron.order.presentation.report;

import com.katonaaron.order.annotations.Column;
import com.katonaaron.order.annotations.GeneratedValue;
import com.katonaaron.order.annotations.Id;
import com.katonaaron.order.model.Product;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Report generator for orders.
 */
public class OrderReportGenerator {
    /**
     * Object used for generating reports.
     */
    private final ReportGenerator<Order> reportGenerator;

    /**
     * Initializes the fields.
     */
    public OrderReportGenerator() {
        reportGenerator = new ReportGenerator<>(Order.class);
    }

    /**
     * Associates the products to the orders, calculates the totals, and generates reports.
     *
     * @param orders   orders to be reported.
     * @param products list of products.
     */
    public void generateReport(List<com.katonaaron.order.model.Order> orders, List<Product> products) {
        final Map<String, Double> productToUnitPrice = products.stream()
                .collect(Collectors.toMap(Product::getName, Product::getUnitPrice));
        final List<Order> orderDTOList = orders.stream()
                .map(order -> new Order(
                        order.getId(),
                        order.getClientName(),
                        order.getProductName(),
                        order.getQuantity(),
                        productToUnitPrice.get(order.getProductName()))
                )
                .collect(Collectors.toList());
        reportGenerator.generateReport(orderDTOList);
    }

    /**
     * Gets the id of the next report.
     *
     * @return id of the next report.
     */
    public int getReportId() {
        return reportGenerator.getReportId();
    }

    /**
     * Increments the id of the next report.
     */
    public void incrementReportId() {
        reportGenerator.incrementReportId();
    }

    /**
     * Data Transfer Object which holds the total sum besides the fields of {@link com.katonaaron.order.model.Order}.
     */
    private static class Order {
        @Id
        @GeneratedValue
        @Column
        private Integer id;

        @Column
        private String clientName;

        @Column
        private String productName;

        @Column
        private Integer quantity;

        @Column
        private Double total;

        public Order(Integer id, String clientName, String productName, Integer quantity, Double unitPrice) {
            this.id = id;
            this.clientName = clientName;
            this.productName = productName;
            this.quantity = quantity;
            this.total = quantity * unitPrice;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }
    }
}
