package com.katonaaron.order.presentation.controller;

import com.katonaaron.order.command.Command;

/**
 * Represents a controller which executes a command.
 */
public interface CommandController {
    /**
     * Executes a command.
     *
     * @param command command to be executed.
     */
    void execute(Command command);
}
