package com.katonaaron.order.presentation.controller;

import com.katonaaron.order.bll.service.Service;
import com.katonaaron.order.command.Command;
import com.katonaaron.order.exception.InvalidOptionException;
import com.katonaaron.order.model.Client;
import com.katonaaron.order.model.StockItem;

/**
 * Represents a controller which executes a DELETE command.
 */
public class DeleteController implements CommandController {
    /**
     * Service for database operations with clients.
     */
    private final Service<Client, String> clientService;
    /**
     * Service for database operations with stock items.
     */
    private final Service<StockItem, String> stockItemService;

    /**
     * Initializes the fields.
     *
     * @param clientService    service for database operations with clients.
     * @param stockItemService service for database operations with stock items.
     */
    public DeleteController(Service<Client, String> clientService, Service<StockItem, String> stockItemService) {
        this.clientService = clientService;
        this.stockItemService = stockItemService;
    }

    /**
     * Executes a DELETE command. Deletes an entity from the database.
     *
     * @param command command to be executed.
     */
    @Override
    public void execute(Command command) {
        if (command.getEntityName().equals("client")) {
            if (command.getParameters().size() < 1) {
                throw new InvalidOptionException("client name must be specified");
            }
            clientService.deleteById(command.getParameters().get(0));
        } else if (command.getEntityName().equals("product")) {
            if (command.getParameters().size() < 1) {
                throw new InvalidOptionException("product name must be specified");
            }
            stockItemService.deleteById(command.getParameters().get(0));
        }
    }
}
