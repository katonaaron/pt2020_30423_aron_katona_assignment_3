package com.katonaaron.order.presentation.controller;

import com.katonaaron.order.bll.service.Service;
import com.katonaaron.order.command.Command;
import com.katonaaron.order.exception.InvalidOptionException;
import com.katonaaron.order.model.Client;
import com.katonaaron.order.model.Order;
import com.katonaaron.order.model.Product;
import com.katonaaron.order.model.StockItem;
import com.katonaaron.order.presentation.io.PDFWriter;
import com.katonaaron.order.presentation.report.OrderReportGenerator;
import com.katonaaron.order.util.EntityUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Represents a controller which executes an INSERT command.
 */
public class InsertController implements CommandController {
    /**
     * Service for database operations with clients.
     */
    private final Service<Client, String> clientService;
    /**
     * Service for database operations with products.
     */
    private final Service<Product, String> productService;
    /**
     * Service for database operations with stock items.
     */
    private final Service<StockItem, String> stockItemService;
    /**
     * Service for database operations with orders.
     */
    private final Service<Order, Integer> orderService;
    /**
     * Report generator for orders.
     */
    private final OrderReportGenerator orderReportGenerator;
    /**
     * Utility for writing a message into a PDF.
     */
    private final PDFWriter pdfWriter;

    /**
     * Initializes the fields.
     *
     * @param clientService        service for database operations with clients.
     * @param productService       service for database operations with products.
     * @param stockItemService     service for database operations with stock items.
     * @param orderService         service for database operations with orders.
     * @param orderReportGenerator report generator for orders.
     * @param pdfWriter            utility for writing a message into a PDF.
     */
    public InsertController(
            Service<Client, String> clientService,
            Service<Product, String> productService,
            Service<StockItem, String> stockItemService,
            Service<Order, Integer> orderService,
            OrderReportGenerator orderReportGenerator, PDFWriter pdfWriter) {
        this.clientService = clientService;
        this.productService = productService;
        this.stockItemService = stockItemService;
        this.orderService = orderService;
        this.orderReportGenerator = orderReportGenerator;
        this.pdfWriter = pdfWriter;
    }

    /**
     * Executes an INSERT command. Inserts an entity into the database.
     *
     * @param command command to be executed.
     */
    @Override
    public void execute(Command command) {
        switch (command.getEntityName()) {
            case "client":
                Client client = EntityUtil.createEntity(Client.class, command.getParameters());
                clientService.save(client);
                break;
            case "product":
                insertProduct(command);
                break;
            case "order":
                insertOrder(command);
                break;
            default:
                throw new InvalidOptionException();
        }
    }

    /**
     * Inserts a product to the database.
     *
     * @param command command to be executed.
     */
    private void insertProduct(Command command) {
        List<String> fields = new ArrayList<>(command.getParameters());
        int quantity = Integer.parseInt(fields.get(1));
        fields.remove(1);

        Product product = EntityUtil.createEntity(Product.class, fields);
        Product savedProduct;
        try {
            savedProduct = productService.findById(product.getId());
        } catch (NoSuchElementException e) {
            savedProduct = null;
        }
        if (savedProduct == null) {
            product = productService.save(product);
        }

        StockItem stockItem;
        try {
            stockItem = stockItemService.findById(product.getId());
        } catch (NoSuchElementException e) {
            stockItem = null;
        }
        if (stockItem == null) {
            stockItemService.save(new StockItem(product.getName(), quantity));
        } else {
            stockItem.increaseQuantity(quantity);
            stockItemService.save(stockItem);
        }
    }

    /**
     * Verifies the stock, creates a new Order, inserts it into the database and generates a report.
     *
     * @param command command to be executed.
     */
    private void insertOrder(Command command) {
        Order order = EntityUtil.createEntity(Order.class, command.getParameters());
        StockItem stockItem;
        try {
            stockItem = stockItemService.findById(order.getProductName());
        } catch (NoSuchElementException e) {
            stockItem = new StockItem("", -1);
        }
        if (stockItem.getQuantity() < order.getQuantity()) {
            pdfWriter.write("report-order-" + orderReportGenerator.getReportId() + ".pdf", "under-stock");
            orderReportGenerator.incrementReportId();
        } else {
            stockItem.increaseQuantity(-order.getQuantity());
            stockItemService.save(stockItem);
            orderService.save(order);
            orderReportGenerator.generateReport(List.of(order), productService.findAll());
        }
    }
}
