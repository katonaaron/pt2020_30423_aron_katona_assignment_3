package com.katonaaron.order.presentation.controller;

import com.katonaaron.order.bll.service.Service;
import com.katonaaron.order.command.Command;
import com.katonaaron.order.exception.InvalidOptionException;
import com.katonaaron.order.model.Client;
import com.katonaaron.order.model.Order;
import com.katonaaron.order.model.Product;
import com.katonaaron.order.model.StockItem;
import com.katonaaron.order.presentation.report.OrderReportGenerator;
import com.katonaaron.order.presentation.report.ProductReportGenerator;
import com.katonaaron.order.presentation.report.ReportGenerator;

/**
 * Represents a controller which executes a REPORT command.
 */
public class ReportController implements CommandController {
    private final ReportGenerator<Client> clientReportGenerator;
    private final ProductReportGenerator productReportGenerator;
    private final OrderReportGenerator orderReportGenerator;
    private final Service<Client, String> clientService;
    private final Service<Product, String> productService;
    private final Service<StockItem, String> stockItemService;
    private final Service<Order, Integer> orderService;

    public ReportController(
            ReportGenerator<Client> clientReportGenerator,
            ProductReportGenerator productReportGenerator,
            OrderReportGenerator orderReportGenerator, Service<Client, String> clientService,
            Service<Product, String> productService,
            Service<StockItem, String> stockItemService,
            Service<Order, Integer> orderService) {
        this.clientReportGenerator = clientReportGenerator;
        this.productReportGenerator = productReportGenerator;
        this.orderReportGenerator = orderReportGenerator;
        this.clientService = clientService;
        this.productService = productService;
        this.stockItemService = stockItemService;
        this.orderService = orderService;
    }

    @Override
    public void execute(Command command) {
        switch (command.getEntityName()) {
            case "client":
                clientReportGenerator.generateReport(clientService.findAll());
                break;
            case "product":
                productReportGenerator.generateReport(productService.findAll(), stockItemService.findAll());
                break;
            case "order":
                orderReportGenerator.generateReport(orderService.findAll(), productService.findAll());
                break;
            default:
                throw new InvalidOptionException();
        }
    }
}
