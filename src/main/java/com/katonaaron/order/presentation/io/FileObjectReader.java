package com.katonaaron.order.presentation.io;

import com.katonaaron.order.presentation.transformer.StringTransformer;

import java.util.Scanner;

/**
 * Reads object from a text file sequentially, line-by-line.
 *
 * @param <T> type of the object to be read.
 */
public class FileObjectReader<T> implements ObjectReader<T> {
    /**
     * Scanner for reading from the data source.
     */
    private final Scanner scanner;

    /**
     * Opens the file for reading.
     *
     * @param source data source from which the objects fill be read.
     */
    public FileObjectReader(Readable source) {
        this.scanner = new Scanner(source);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next(StringTransformer<T> transformer) throws Exception {
        String line = scanner.nextLine();
        return transformer.reverseTransform(line);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext() {
        return scanner.hasNextLine();
    }

    /**
     * Closes the file.
     */
    @Override
    public void close() {
        scanner.close();
    }
}

