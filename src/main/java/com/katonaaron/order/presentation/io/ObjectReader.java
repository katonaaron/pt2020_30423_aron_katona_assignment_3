package com.katonaaron.order.presentation.io;

import com.katonaaron.order.presentation.transformer.StringTransformer;

/**
 * Reads object from a text based data source sequentially, line-by-line.
 *
 * @param <T> type of the object to be read.
 */
public interface ObjectReader<T> extends AutoCloseable {
    /**
     * Fetches the next object from the data source.
     *
     * @param transformer transforms the object from String to the generic type.
     * @return the next object.
     * @throws Exception on error.
     */
    T next(StringTransformer<T> transformer) throws Exception;

    /**
     * Verifies whether there are moe objects to be read/
     *
     * @return true if indeed there are objects to be read.
     */
    boolean hasNext();
}
