package com.katonaaron.order.presentation.io;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Allows the writing of a string into a PDF file.
 */
public class PDFWriter {

    /**
     * Writes a message into a PDF file.
     *
     * @param fileName name of the PDF file.
     * @param message  message to be written.
     */
    public void write(String fileName, String message) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(fileName));

            document.open();

            document.add(new Paragraph(message));

            document.close();
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        }
    }
}
