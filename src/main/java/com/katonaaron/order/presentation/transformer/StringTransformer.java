package com.katonaaron.order.presentation.transformer;

/**
 * Represents a transformer, which converts an object to/from String.
 *
 * @param <T> type of object.
 */
public interface StringTransformer<T> extends Transformer<T, String> {
    /**
     * Transforms the object to String.
     *
     * @param obj object to be transformed.
     * @return the transformed object.
     */
    @Override
    default String transform(T obj) {
        return obj.toString();
    }

    /**
     * Transforms a string into an object.
     *
     * @param string string to be transformed.
     * @return the transformed object.
     * @throws Exception on error.
     */
    @Override
    T reverseTransform(String string) throws Exception;
}
