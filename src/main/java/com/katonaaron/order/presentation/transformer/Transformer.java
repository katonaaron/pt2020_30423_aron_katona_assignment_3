package com.katonaaron.order.presentation.transformer;

/**
 * Represents a transformer, which converts an object from a type to another type.
 *
 * @param <T> first type.
 * @param <E> second type.
 */
public interface Transformer<T, E> {
    /**
     * Transforms the object from the first type to the second type.
     *
     * @param obj object to be transformed.
     * @return the transformed object.
     * @throws Exception on error.
     */
    E transform(T obj) throws Exception;

    /**
     * Transforms the object from the second type to the first type.
     *
     * @param obj object to be transformed.
     * @return the transformed object.
     * @throws Exception on error.
     */
    T reverseTransform(E obj) throws Exception;
}
