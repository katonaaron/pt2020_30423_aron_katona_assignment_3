package com.katonaaron.order.presentation.transformer;

import com.katonaaron.order.command.Command;
import com.katonaaron.order.command.CommandType;
import com.katonaaron.order.exception.InvalidCommandException;
import com.katonaaron.order.exception.InvalidFormatException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a transformer, which converts a Command to/from String.
 */
public class CommandTransformer implements StringTransformer<Command> {

    /**
     * Transforms a string into a Command.
     *
     * @param line string to be transformed.
     * @return the transformed object.
     * @throws InvalidCommandException if the command is not recognised.
     * @throws InvalidFormatException  if the string has an invalid format.
     */
    @Override
    public Command reverseTransform(String line) throws InvalidCommandException, InvalidFormatException {
        if (line == null || line.isEmpty()) {
            return null;//TODO: check somewhere for null
        }

        String[] parameters = line
                .trim()
                .replace(" +", " ")
                .split(":");

        if (parameters[0].equals("Order")) {
            parameters[0] = "Insert order";
        }

        String[] commandStrings = parameters[0].split(" ");
        if (commandStrings.length != 2) {
            throw new InvalidFormatException("A command and an entity must be specified: " + parameters[0]);
        }

        CommandType commandType;
        try {
            commandType = CommandType.valueOf(commandStrings[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidCommandException(commandStrings[0], e);
        }

        List<String> commandParameters;
        if (parameters.length < 2) {
            commandParameters = List.of();
        } else {
            commandParameters = Arrays.stream(parameters[1].split(","))
                    .map(String::trim)
                    .collect(Collectors.toList());
        }

        return new Command(commandType, commandStrings[1].toLowerCase(), commandParameters);
    }
}
