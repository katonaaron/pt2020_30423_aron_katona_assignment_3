package com.katonaaron.order.util;

import java.util.Set;

/**
 * Utility class for reflection operation with classes.
 */
public final class ClassUtil {
    /**
     * Set of wrapper classes.
     */
    private static final Set<Class<?>> WRAPPER_TYPES = Set.of(
            Boolean.class, Character.class, Byte.class, Short.class, Integer.class,
            Long.class, Float.class, Double.class, Void.class
    );

    /**
     * Private empty constructor
     */
    private ClassUtil() {
    }

    /**
     * Determines whether a class is a wrapper class or not.
     *
     * @param type class
     * @return true if the class is a wrapper class.
     */
    public static boolean isWrapper(Class<?> type) {
        return WRAPPER_TYPES.contains(type);
    }
}
