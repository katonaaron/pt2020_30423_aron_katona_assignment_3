package com.katonaaron.order.util;

import com.katonaaron.order.annotations.*;
import com.katonaaron.order.exception.EntityReflectionException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class for reflection operations with @Entity classes.
 */
public final class EntityUtil {

    /**
     * Private empty constructor
     */
    private EntityUtil() {

    }

    /**
     * Instantiates an entity object determined by the given field values.
     *
     * @param type        class of the entity.
     * @param fieldValues values of the fields as Strings.
     * @param <T>         type of the entity
     * @return the instantiated entity object.
     */
    public static <T> T createEntity(Class<T> type, List<String> fieldValues) {
        T model;
        try {
            model = type.getConstructor().newInstance();

            List<Field> fields = Arrays.stream(type.getDeclaredFields())
                    .filter(field -> !field.isAnnotationPresent(GeneratedValue.class))
                    .collect(Collectors.toList());
            if (fieldValues.size() != fields.size()) {
                throw new EntityReflectionException("Invalid number of fields");
            }
            int i = 0;
            for (Field field : fields) {
                field.setAccessible(true);
                Class<?> fieldType = field.getType();

                Object fieldValue;
                if (fieldType.equals(String.class)) {
                    fieldValue = fieldValues.get(i);
                } else if (ClassUtil.isWrapper(fieldType)) {
                    fieldValue = fieldType
                            .getMethod("valueOf", String.class)
                            .invoke(null, fieldValues.get(i));
                } else {
                    fieldValue = field.get(model);
                    //TODO: get from database
                }
                field.set(model, fieldValue);
                i++;
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new EntityReflectionException(e);
        }
        return model;
    }

    /**
     * Returns the @Id field of the entity
     *
     * @param type class of the entity
     * @param <T>  type of the entity
     * @return the field of the entity marked with @Id
     */
    public static <T> Field getPrimaryKey(Class<T> type) {
        final Field[] fields = type.getDeclaredFields();
        List<Field> result = Stream.concat(
                Arrays.stream(fields)
                        .filter(field -> field.isAnnotationPresent(MapsId.class))
                        .map(field -> getPrimaryKey(field.getType())),
                Arrays.stream(fields)
                        .filter(field -> field.isAnnotationPresent(Id.class))
        ).collect(Collectors.toList());

        if (result.size() != 1) {
            throw new EntityReflectionException("Exactly 1 @Id or @MapsId field must be specified in the class " + type.getName());
        }

        return result.get(0);
    }

    /**
     * Returns the name of a field. If marked with @Column( name = "..."), the specified name will be used.
     *
     * @param field the field.
     * @return the name of the field.
     */
    public static String getColumnName(Field field) {
        final Column annotation = field.getAnnotation(Column.class);

        if (annotation == null || annotation.name().equals("")) {
            return field.getName();
        } else {
            return annotation.name();
        }
    }

    /**
     * Returns the name of  the database table associated with a class marked with @Entity.
     * If @Table(name = "...") is present, the specified name will be used.
     *
     * @param type class of the entity
     * @param <T>  type of the entity
     * @return name of the table
     */
    public static <T> String getTableName(Class<T> type) {
        final Table annotation = type.getAnnotation(Table.class);

        if (annotation == null || annotation.name().equals("")) {
            return type.getSimpleName().toLowerCase();
        } else {
            return annotation.name();
        }
    }

    /**
     * Returns all the fields of an entity, marked with @Column.
     *
     * @param type class of the entity.
     * @param <T>  type of the entity.
     * @return list of fields marked with @Column.
     */
    public static <T> List<Field> getColumns(Class<T> type) {
        return Arrays.stream(type.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Column.class))
                .collect(Collectors.toList());
    }

    /**
     * Returns all the fields of an entity, marked with @Column and @GeneratedValue.
     *
     * @param type class of the entity.
     * @param <T>  type of the entity.
     * @return list of fields marked with @Column and @GeneratedValue.
     */
    public static <T> List<Field> getGeneratedColumns(Class<T> type) {
        return Arrays.stream(type.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Column.class) && field.isAnnotationPresent(GeneratedValue.class))
                .collect(Collectors.toList());
    }

}
