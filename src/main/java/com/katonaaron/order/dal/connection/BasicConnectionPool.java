package com.katonaaron.order.dal.connection;

import com.katonaaron.order.exception.MaxPoolSizeReachedException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a pool of database connections, able to provide, release and close its connections.
 * <br>
 * Creates a limited number of connections and reuses them after they are released.
 */
public class BasicConnectionPool implements ConnectionPool {
    /**
     * Initial size of the connection pool.
     */
    private static final int INITIAL_POOL_SIZE = 5;
    /**
     * Maximum size of the connection pool.
     */
    private static final int MAX_POOL_SIZE = 15;
    /**
     * Database connection string.
     */
    private final String url;
    /**
     * Database user.
     */
    private final String user;
    /**
     * Database password.
     */
    private final String password;
    /**
     * List of connections that represents the connection pool
     */
    private final List<Connection> connectionPool;
    /**
     * List of connections which are in use and taken form the connection pool.
     */
    private final List<Connection> usedConnections = new ArrayList<>();

    /**
     * Constructor for setting the field values.
     *
     * @param url            database connection string.
     * @param user           database user.
     * @param password       database password.
     * @param connectionPool list of connections that represents the connection pool.
     */
    private BasicConnectionPool(String url, String user, String password, List<Connection> connectionPool) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.connectionPool = connectionPool;
    }

    /**
     * Factory method for getting a new connection pool.
     *
     * @param url      database connection string.
     * @param user     database user.
     * @param password database password.
     * @return new connection pool.
     * @throws SQLException if the database connections could not be opened.
     */
    public static BasicConnectionPool create(String url, String user, String password) throws SQLException {
        List<Connection> pool = new ArrayList<>(INITIAL_POOL_SIZE);
        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            pool.add(createConnection(url, user, password));
        }
        return new BasicConnectionPool(url, user, password, pool);
    }

    /**
     * Creates a new database connection.
     *
     * @param url      database connection string.
     * @param user     database user.
     * @param password database password.
     * @return new database connection.
     * @throws SQLException if the connection could not be opened.
     */
    private static Connection createConnection(String url, String user, String password) throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Connection getConnection() throws SQLException {
        if (connectionPool.isEmpty()) {
            if (usedConnections.size() < MAX_POOL_SIZE) {
                connectionPool.add(createConnection(url, user, password));
            } else {
                throw new MaxPoolSizeReachedException();
            }
        }
        Connection connection = connectionPool.remove(connectionPool.size() - 1);
        usedConnections.add(connection);
        return connection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean releaseConnection(Connection connection) {
        connectionPool.add(connection);
        return usedConnections.remove(connection);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUrl() {
        return url;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUser() {
        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSize() {
        return connectionPool.size() + usedConnections.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void shutdown() throws SQLException {
        usedConnections.forEach(this::releaseConnection);
        for (Connection connection : connectionPool) {
            connection.close();
        }
        connectionPool.clear();
    }
}
