package com.katonaaron.order.dal.connection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Represents a pool of database connections, able to provide, release and close its connections.
 */
public interface ConnectionPool {
    /**
     * Gets a unique database connection from the connection pool.
     *
     * @return database connection
     * @throws SQLException if the connection could not be created.
     */
    Connection getConnection() throws SQLException;

    /**
     * Returns the connection to the connection pool
     *
     * @param connection database connection given by {@link #getConnection()}.
     * @return true if the connection was successfully released.
     */
    boolean releaseConnection(Connection connection);

    /**
     * Gets the connection string.
     *
     * @return connection string
     */
    String getUrl();

    /**
     * Gets the database user.
     *
     * @return database user.
     */
    String getUser();


    /**
     * Gets the database password.
     *
     * @return database password.
     */
    String getPassword();

    /**
     * Gets the size of the connection pool.
     *
     * @return size of the connection pool.
     */
    int getSize();

    /**
     * Closes all database connections.
     *
     * @throws SQLException if the connection could not be closed.
     */
    void shutdown() throws SQLException;
}
