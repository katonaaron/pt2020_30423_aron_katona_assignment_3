package com.katonaaron.order.dal.dao;

import com.katonaaron.order.annotations.Entity;
import com.katonaaron.order.annotations.GeneratedValue;
import com.katonaaron.order.dal.connection.ConnectionPool;
import com.katonaaron.order.exception.ClassNotEntityException;
import com.katonaaron.order.exception.DAOException;
import com.katonaaron.order.exception.EntityReflectionException;
import com.katonaaron.order.util.ClassUtil;
import com.katonaaron.order.util.EntityUtil;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.katonaaron.order.util.EntityUtil.getColumnName;

/**
 * Represents a generic Data Access Object for executing SQL queries and statements for the given entity.
 *
 * @param <T>  type of the entity.
 * @param <ID> type of the primary key of the entity.
 */
public abstract class AbstractDAO<T, ID> implements DAO<T, ID> {
    /**
     * Logger associated with the class.
     */
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    /**
     * Connection pool.
     */
    private final ConnectionPool connectionPool;

    /**
     * Type of the entity with which the DAO is associated.
     */
    private final Class<T> type;
    /**
     * Name of the database table associated with the entity.
     */
    private final String tableName;
    /**
     * Name of the primary key of the database table associated with the entity.
     */
    private final Field primaryKey;
    /**
     * List of columns of the database table associated with the entity.
     */
    private final List<Field> columns;
    /**
     * List of columns of the database table which are generated after insertion and needed to be fetched afterwards.
     */
    private final List<Field> generatedColumns;
    /**
     * Names of the generated fields.
     */
    private final String[] generatedColumnNames;

    /* Query strings */
    /**
     * SELECT query for finding all entities.
     */
    private final String findAllQuery;
    /**
     * SELECT query for finding an entity by an id.
     */
    private final String findByIdQuery;
    /**
     * INSERT query for inserting a new entity.
     */
    private final String insertStatement;
    /**
     * UPDATE query for updating an existing entity.
     */
    private final String updateStatement;
    /**
     * DELETE query for deleting an existing entity.
     */
    private final String deleteStatement;

    /**
     * Constructor for initializing all fields.
     *
     * @param connectionPool connection pool from which the connections are taken.
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        if (!type.isAnnotationPresent(Entity.class)) {
            throw new ClassNotEntityException(type.getName());//TODO: where to catch
        }
        tableName = EntityUtil.getTableName(type);
        primaryKey = EntityUtil.getPrimaryKey(type);
        columns = EntityUtil.getColumns(type);
        if (columns.size() == 0) {
            throw new EntityReflectionException(type.getName() + " has no columns");
        }
        generatedColumns = EntityUtil.getGeneratedColumns(type);
        generatedColumnNames = generatedColumns.stream().map(EntityUtil::getColumnName).toArray(String[]::new);

        findAllQuery = "SELECT * FROM " + tableName + ";";
        findByIdQuery = "SELECT * FROM " + tableName + " WHERE " + getColumnName(primaryKey) + " = ?;";
        insertStatement = createInsertStatement();
        updateStatement = createUpdateStatement();
        deleteStatement = "DELETE FROM " + tableName + " WHERE " + getColumnName(primaryKey) + " = ?;";
    }

    /**
     * {@inheritDoc}
     */
    public List<T> findAll() {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(findAllQuery)
            ) {
                return createObjects(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, getClass().getName() + ": findAll: " + e.getMessage());
        } finally {
            connectionPool.releaseConnection(connection);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public T findById(ID id) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            List<T> results;

            try (PreparedStatement statement = connection.prepareStatement(findByIdQuery)) {
                statement.setObject(1, id);

                try (ResultSet resultSet = statement.executeQuery()) {
                    results = createObjects(resultSet);
                }
            }
            if (null == results || results.isEmpty())
                return null;
            return results.get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, getClass().getName() + ": findById: " + e.getMessage());
        } finally {
            connectionPool.releaseConnection(connection);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public T insert(T t) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            ID id;

            try (PreparedStatement statement = connection.prepareStatement(insertStatement, generatedColumnNames)
            ) {
                int i = 1;
                for (Field column : columns) { //TODO: relationships
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getColumnName(column), type);
                    Object value = propertyDescriptor.getReadMethod().invoke(t);
                    statement.setObject(i++, value);
                }

                if (statement.executeUpdate() < 1) {
                    LOGGER.log(Level.WARNING, getClass().getName() + ": insert: executeUpdate() rowcount < 1");
                    return null;
                }

                if (generatedColumns.size() != 0) {
                    try (ResultSet resultSet = statement.getGeneratedKeys()) {
                        i = 1;
                        for (Field column : generatedColumns) {
                            if (resultSet.next()) {
                                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getColumnName(column), type);

                                Class<?> fieldType = column.getType();
                                Object result = resultSet.getObject(i++);
                                Object fieldValue;
                                if (fieldType.equals(String.class)) {
                                    fieldValue = result.toString();
                                } else if (ClassUtil.isWrapper(fieldType)) {
                                    fieldValue = fieldType
                                            .getMethod("valueOf", String.class)
                                            .invoke(null, result.toString());
                                } else {
                                    fieldValue = column.get(t);
                                    //TODO: get from database
                                }
                                propertyDescriptor.getWriteMethod().invoke(t, fieldValue);
                            } else {
                                throw new DAOException("Did not receive all the values of the generated columns");
                            }
                        }
                    }
                }
            }
            return t;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, getClass().getName() + ": insert: " + e.getMessage());
        } catch (IllegalAccessException | InvocationTargetException | IntrospectionException | NoSuchMethodException e) {
            throw new EntityReflectionException(e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public T update(T t) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();
            Object id;

            try (PreparedStatement statement = connection.prepareStatement(updateStatement)) {
                int i = 1;
                for (Field column : columns) { //TODO: relationships
                    if (column.isAnnotationPresent(GeneratedValue.class)) {
                        continue;
                    }
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getColumnName(column), type);
                    Object value = propertyDescriptor.getReadMethod().invoke(t);
                    statement.setObject(i++, value);
                }

                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(getColumnName(primaryKey), type);
                Method method = propertyDescriptor.getReadMethod();
                id = method.invoke(t);
                statement.setObject(i, id);

                if (statement.executeUpdate() < 1) {
                    LOGGER.log(Level.WARNING, getClass().getName() + ": update: executeUpdate() rowcount < 1");
                    return null;
                }
            }
            return findById((ID) id);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, getClass().getName() + ": update: " + e.getMessage());
        } catch (IllegalAccessException | InvocationTargetException | IntrospectionException e) {
            throw new EntityReflectionException(e);
        } finally {
            connectionPool.releaseConnection(connection);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public boolean deleteById(ID id) {
        Connection connection = null;
        try {
            connection = connectionPool.getConnection();

            try (PreparedStatement statement = connection.prepareStatement(deleteStatement)) {
                statement.setObject(1, id);

                if (statement.executeUpdate() < 1) {
                    LOGGER.log(Level.WARNING, getClass().getName() + ": deleteById: executeUpdate() rowcount < 1");
                    return false;
                }
                return true;
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, getClass().getName() + ": deleteById: " + e.getMessage());
        } finally {
            connectionPool.releaseConnection(connection);
        }
        return false;
    }


    /**
     * Creates an INSERT SQL statement.
     *
     * @return sql statement.
     */
    private String createInsertStatement() {
        return "INSERT INTO " + tableName +
                " (" +
                columns.stream()
                        .map(EntityUtil::getColumnName)
                        .reduce((a, b) -> a + ", " + b)
                        .orElseThrow(() -> new EntityReflectionException("could not get columns")) +
                ") VALUES (" +
                String.join(",", Collections.nCopies(columns.size(), "?")) +
                ");";
    }

    /**
     * Creates an UPDATE SQL statement.
     *
     * @return sql statement.
     */
    private String createUpdateStatement() {
        return "UPDATE " + tableName + " SET " +
                columns.stream()
                        .filter(column -> !column.isAnnotationPresent(GeneratedValue.class))
                        .map(field -> getColumnName(field) + " = ?")
                        .reduce((a, b) -> a + ", " + b)
                        .orElseThrow(() -> new EntityReflectionException("could not get columns")) +
                " WHERE " + getColumnName(primaryKey) + " = ?;";
    }

    /**
     * Transforms the result set into an object.
     *
     * @param resultSet the result set containing an entity.
     * @return the constructed object.
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> results = new ArrayList<>();

        try {
            while (resultSet.next()) {
                T instance = type.getDeclaredConstructor().newInstance();

                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }

                results.add(instance);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, getClass().getName() + ": createObjects " + e.getMessage());
            return null;
        } catch (IllegalAccessException | InvocationTargetException | IntrospectionException | NoSuchMethodException | InstantiationException e) {
            throw new EntityReflectionException(e);
        }

        return results;
    }
}
