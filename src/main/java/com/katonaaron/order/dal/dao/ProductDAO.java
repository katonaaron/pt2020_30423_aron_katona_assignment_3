package com.katonaaron.order.dal.dao;

import com.katonaaron.order.dal.connection.ConnectionPool;
import com.katonaaron.order.model.Product;

/**
 * Represents a Data Access Object for executing SQL queries and statements for the Product entity.
 */
public class ProductDAO extends AbstractDAO<Product, String> {
    /**
     * Constructor which calls the parent constructor: {@link AbstractDAO#AbstractDAO(ConnectionPool)}.
     *
     * @param connectionPool the connection pool from which the database connections are taken.
     */
    public ProductDAO(ConnectionPool connectionPool) {
        super(connectionPool);
    }
}
