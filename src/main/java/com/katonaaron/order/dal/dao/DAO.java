package com.katonaaron.order.dal.dao;

import java.util.List;

/**
 * Represents a Data Access Object for executing SQL queries and statements for the given entity.
 *
 * @param <T>  type of the entity.
 * @param <ID> type of the primary key of the entity.
 */
public interface DAO<T, ID> {
    /**
     * Gets all entities from the database.
     *
     * @return list of entities.
     */
    List<T> findAll();

    /**
     * Finds an entity from the database that correspond to the given id.
     *
     * @param id id of the entity.
     * @return the found entity, null if not found.
     */
    T findById(ID id);

    /**
     * Inserts a new entity to the database.
     *
     * @param t the entity to be inserted.
     * @return the newly inserted entity, having the same field values as the database table. Or null if it could not be saved.
     * @throws com.katonaaron.order.exception.EntityReflectionException if the entity does not correspond to the
     *                                                                  required format, resulting in an error, which occurred during a reflection operation.
     */
    T insert(T t);

    /**
     * Updates a database table record of an existing entity.
     *
     * @param t the entity to be updated.
     * @return the updated entity, having the same field values as the database table. Or null if it could not be saved.
     * @throws com.katonaaron.order.exception.EntityReflectionException if the entity does not correspond to the
     *                                                                  required format, resulting in an error, which occurred during a reflection operation.
     */
    T update(T t);

    /**
     * Deletes an entity from the database given by its id.
     *
     * @param id the id of the entity to be deleted.
     * @return true if the entity was successfully deleted.
     */
    boolean deleteById(ID id);
}
