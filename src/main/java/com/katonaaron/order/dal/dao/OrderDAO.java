package com.katonaaron.order.dal.dao;

import com.katonaaron.order.dal.connection.ConnectionPool;
import com.katonaaron.order.model.Order;

/**
 * Represents a Data Access Object for executing SQL queries and statements for the Order entity.
 */
public class OrderDAO extends AbstractDAO<Order, Integer> {
    /**
     * Constructor which calls the parent constructor: {@link AbstractDAO#AbstractDAO(ConnectionPool)}.
     *
     * @param connectionPool the connection pool from which the database connections are taken.
     */
    public OrderDAO(ConnectionPool connectionPool) {
        super(connectionPool);
    }
}
