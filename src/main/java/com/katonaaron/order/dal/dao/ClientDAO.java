package com.katonaaron.order.dal.dao;

import com.katonaaron.order.dal.connection.ConnectionPool;
import com.katonaaron.order.model.Client;

/**
 * Represents a Data Access Object for executing SQL queries and statements for the Client entity.
 */
public class ClientDAO extends AbstractDAO<Client, String> {
    /**
     * Constructor which calls the parent constructor: {@link AbstractDAO#AbstractDAO(ConnectionPool)}.
     *
     * @param connectionPool the connection pool from which the database connections are taken.
     */
    public ClientDAO(ConnectionPool connectionPool) {
        super(connectionPool);
    }
}
