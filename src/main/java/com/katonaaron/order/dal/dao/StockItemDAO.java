package com.katonaaron.order.dal.dao;

import com.katonaaron.order.dal.connection.ConnectionPool;
import com.katonaaron.order.model.StockItem;

/**
 * Represents a Data Access Object for executing SQL queries and statements for the StockItem entity.
 */
public class StockItemDAO extends AbstractDAO<StockItem, String> {
    /**
     * Constructor which calls the parent constructor: {@link AbstractDAO#AbstractDAO(ConnectionPool)}.
     *
     * @param connectionPool the connection pool from which the database connections are taken.
     */
    public StockItemDAO(ConnectionPool connectionPool) {
        super(connectionPool);
    }
}
