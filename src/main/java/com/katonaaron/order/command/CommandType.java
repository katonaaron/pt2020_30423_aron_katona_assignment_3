package com.katonaaron.order.command;

/**
 * Represents all the types a command can have.
 */
public enum CommandType {
    /**
     * Inserts a new entity instance.
     */
    INSERT,
    /**
     * Deletes an existing entity instance.
     */
    DELETE,
    /**
     * Generates a report about an entity.
     */
    REPORT
}
