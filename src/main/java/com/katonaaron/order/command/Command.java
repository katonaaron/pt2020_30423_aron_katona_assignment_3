package com.katonaaron.order.command;

import java.util.List;

/**
 * Represents an immutable command that needs to be executed.
 */
public class Command {
    /**
     * Type of the command.
     */
    private final CommandType type;
    /**
     * Name of the entity on which the command will be executed.
     */
    private final String entityName;
    /**
     * Command parameters
     */
    private final List<String> parameters;

    /**
     * Initializes the fields.
     *
     * @param type       type of the command.
     * @param entityName name of the entity on which the command will be executed.
     * @param parameters command parameters
     */
    public Command(CommandType type, String entityName, List<String> parameters) {
        this.type = type;
        this.entityName = entityName;
        this.parameters = List.copyOf(parameters);
    }

    /**
     * Gets the type of the command.
     *
     * @return command type.
     */
    public CommandType getType() {
        return type;
    }

    /**
     * Gets the name of the entity on which the command will be executed.
     *
     * @return entity name.
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * Gets the parameters of the command.
     *
     * @return command parameters.
     */
    public List<String> getParameters() {
        return List.copyOf(parameters);
    }

    /**
     * Returns a string representation of the object
     *
     * @return the string representation.
     */
    @Override
    public String toString() {
        return "Command{" +
                "type=" + type +
                ", entityName='" + entityName + '\'' +
                ", parameters=" + parameters +
                '}';
    }
}
