package com.katonaaron.order.command;

import com.katonaaron.order.presentation.controller.CommandController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A manager object which receives and executes commands.
 */
public class CommandManager {
    /**
     * List of commands that need to be executed
     */
    private final List<Command> commandList;
    /**
     * A map with Controller objects associated to the type of commands they support.
     */
    private final Map<CommandType, CommandController> controllerMap;

    /**
     * Initializes the fields with the parameters.
     *
     * @param controllerMap map with Controller objects associated to the type of commands they support.
     */
    public CommandManager(Map<CommandType, CommandController> controllerMap) {
        this.controllerMap = controllerMap;
        commandList = new ArrayList<>();
    }

    /**
     * Adds a command to the list of pending commands.
     *
     * @param command command to be added.
     */
    public void addCommand(Command command) {
        Objects.requireNonNull(command);
        commandList.add(command);
    }

    /**
     * Executes all the comands and removes them from the list.
     */
    public void executeCommands() {
        commandList.forEach(command -> controllerMap.get(command.getType()).execute(command));
        commandList.clear();
    }
}
