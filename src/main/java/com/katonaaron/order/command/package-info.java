/**
 * Classes for representing and executing commands.
 */
package com.katonaaron.order.command;
