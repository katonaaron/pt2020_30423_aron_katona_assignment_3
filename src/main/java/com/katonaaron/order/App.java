package com.katonaaron.order;

import com.katonaaron.order.bll.service.ClientService;
import com.katonaaron.order.bll.service.OrderService;
import com.katonaaron.order.bll.service.ProductService;
import com.katonaaron.order.bll.service.StockItemService;
import com.katonaaron.order.command.Command;
import com.katonaaron.order.command.CommandManager;
import com.katonaaron.order.command.CommandType;
import com.katonaaron.order.dal.connection.BasicConnectionPool;
import com.katonaaron.order.dal.connection.ConnectionPool;
import com.katonaaron.order.dal.dao.ClientDAO;
import com.katonaaron.order.dal.dao.OrderDAO;
import com.katonaaron.order.dal.dao.ProductDAO;
import com.katonaaron.order.dal.dao.StockItemDAO;
import com.katonaaron.order.model.Client;
import com.katonaaron.order.presentation.controller.CommandController;
import com.katonaaron.order.presentation.controller.DeleteController;
import com.katonaaron.order.presentation.controller.InsertController;
import com.katonaaron.order.presentation.controller.ReportController;
import com.katonaaron.order.presentation.io.FileObjectReader;
import com.katonaaron.order.presentation.io.ObjectReader;
import com.katonaaron.order.presentation.io.PDFWriter;
import com.katonaaron.order.presentation.report.OrderReportGenerator;
import com.katonaaron.order.presentation.report.ProductReportGenerator;
import com.katonaaron.order.presentation.report.ReportGenerator;
import com.katonaaron.order.presentation.transformer.CommandTransformer;
import com.katonaaron.order.presentation.transformer.StringTransformer;

import java.io.*;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import static java.lang.System.exit;

/**
 * Main class. Reads files and configurations, solves dependencies, instantiates objects and executes commands.
 */
public class App {
    private static final String PROPERTIES_PATH = "/app.properties";

    private static ConnectionPool connectionPool;
    private static CommandManager commandManager;

    /**
     * Application start.
     *
     * @param args commandline argument: the path to the input file.
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Required parameter: <input_path>");
            exit(-1);
        }

        Properties properties = getProperties(PROPERTIES_PATH);

        instantiate(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        readCommands(args[0]);
        commandManager.executeCommands();

        try {
            connectionPool.shutdown();
        } catch (SQLException e) {
            System.err.println("ERROR: connection pool could not be shut down: " + e);
            exit(-1);
        }
    }

    /**
     * Instantiating objects doing dependency injection.
     *
     * @param url      database connection string.
     * @param user     database user.
     * @param password database password.
     */
    private static void instantiate(String url, String user, String password) {
        try {
            connectionPool = BasicConnectionPool.create(url, user, password);
        } catch (SQLException e) {
            System.err.println("ERROR: connection pool could not be created: " + e);
            exit(-1);
        }

        ClientDAO clientDAO = new ClientDAO(connectionPool);
        ProductDAO productDAO = new ProductDAO(connectionPool);
        StockItemDAO stockItemDAO = new StockItemDAO(connectionPool);
        OrderDAO orderDAO = new OrderDAO(connectionPool);

        ClientService clientService = new ClientService(clientDAO);
        ProductService productService = new ProductService(productDAO);
        StockItemService stockItemService = new StockItemService(stockItemDAO);
        OrderService orderService = new OrderService(orderDAO);

        ReportGenerator<Client> clientReportGenerator = new ReportGenerator<>(Client.class);
        ProductReportGenerator productReportGenerator = new ProductReportGenerator();
        OrderReportGenerator orderReportGenerator = new OrderReportGenerator();

        PDFWriter pdfWriter = new PDFWriter();

        Map<CommandType, CommandController> commandControllerMap = Map.of(
                CommandType.INSERT, new InsertController(clientService, productService, stockItemService, orderService, orderReportGenerator, pdfWriter),
                CommandType.REPORT, new ReportController(clientReportGenerator, productReportGenerator, orderReportGenerator, clientService, productService, stockItemService, orderService),
                CommandType.DELETE, new DeleteController(clientService, stockItemService)
        );

        commandManager = new CommandManager(commandControllerMap);
    }

    /**
     * Gets the app properties from the resource files.
     *
     * @param path path to the properties file inside the resources.
     * @return application properties.
     */
    private static Properties getProperties(String path) {
        Properties props = new Properties();
        try {
            InputStream inputStream = App.class.getResourceAsStream(path);
            if (inputStream == null) {
                throw new FileNotFoundException(path);
            }
            props.load(inputStream);
        } catch (IOException e) {
            System.err.println("ERROR: properties resource file could not be read: " + e);
            exit(-1);
        }
        return props;
    }

    /**
     * Reads the commands from the input files and appends them to the command manager.
     *
     * @param path path to the input file.
     */
    private static void readCommands(String path) {
        StringTransformer<Command> transformer = new CommandTransformer();

        try (ObjectReader<Command> reader
                     = new FileObjectReader<>(new BufferedReader(new FileReader(path)))) {
            while (reader.hasNext()) {
                commandManager.addCommand(reader.next(transformer));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
