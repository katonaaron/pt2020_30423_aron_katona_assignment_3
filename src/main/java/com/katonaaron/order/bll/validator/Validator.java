package com.katonaaron.order.bll.validator;

import com.katonaaron.order.exception.ValidationException;

/**
 * Represents a validator that verifies the correct value of a given object.
 *
 * @param <T> type of the entity.
 */
public interface Validator<T> {
    /**
     * Validates one or more fields of an object.
     *
     * @param obj the object to be validated.
     * @throws ValidationException if validation failed.
     */
    void validate(T obj) throws ValidationException;
}
