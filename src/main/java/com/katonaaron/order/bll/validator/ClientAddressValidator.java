package com.katonaaron.order.bll.validator;

import com.katonaaron.order.exception.InvalidNameException;
import com.katonaaron.order.exception.ValidationException;
import com.katonaaron.order.model.Client;

/**
 * Represents a validator that verifies the address of a Client.
 */
public class ClientAddressValidator implements Validator<Client> {
    /**
     * Validates the address of a client.
     *
     * @param client client to be validated.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validate(Client client) {
        if (client.getName().isEmpty()) {
            throw new InvalidNameException("address is empty");
        }
    }
}
