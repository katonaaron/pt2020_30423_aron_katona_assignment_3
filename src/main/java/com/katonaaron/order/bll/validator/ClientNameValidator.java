package com.katonaaron.order.bll.validator;

import com.katonaaron.order.exception.InvalidNameException;
import com.katonaaron.order.exception.ValidationException;
import com.katonaaron.order.model.Client;

/**
 * Represents a validator that verifies the name of a Client.
 */
public class ClientNameValidator implements Validator<Client> {
    /**
     * Validates the name of a client.
     *
     * @param client client to be validated.
     * @throws ValidationException if validation failed.
     */
    @Override
    public void validate(Client client) throws ValidationException {
        if (client.getName().isEmpty()) {
            throw new InvalidNameException("name is empty");
        }
    }
}
