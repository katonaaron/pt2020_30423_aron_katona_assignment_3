package com.katonaaron.order.bll.service;

import com.katonaaron.order.bll.validator.Validator;
import com.katonaaron.order.dal.dao.DAO;
import com.katonaaron.order.exception.OperationFailedException;
import com.katonaaron.order.model.AbstractEntity;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Represents a generic service for persisting an entity with the database.
 *
 * @param <T>  Type of the entity.
 * @param <ID> Type of the primary key of the entity.
 */
public abstract class AbstractService<T extends AbstractEntity, ID> implements Service<T, ID> {
    /**
     * List of validators that are called when an entity is about to be saved.
     */
    private final List<Validator<T>> validators;
    /**
     * DAO used for entity persistence.
     */
    private final DAO<T, ID> dao;

    /**
     * Constructor for field initialization.
     *
     * @param validators list of validators that are called when an entity is about to be saved.
     * @param dao        DAO used for entity persistence.
     */
    public AbstractService(List<Validator<T>> validators, DAO<T, ID> dao) {
        this.validators = validators;
        this.dao = dao;
    }

    /**
     * Validates an entity.
     *
     * @param t the entity.
     * @throws IllegalArgumentException if the object is null.
     */
    private void validate(T t) {
        if (t == null) {
            throw new IllegalArgumentException("t is null");
        }
        validators.forEach(validator -> validator.validate(t));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> findAll() {
        List<T> result = dao.findAll();
        if (result == null) {
            throw new OperationFailedException("could not find all entities");
        }
        result.forEach(AbstractEntity::setSaved);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T findById(ID id) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }

        T entity = dao.findById(id);
        if (entity == null) {
            throw new NoSuchElementException("entity with id = \"" + id + "\" was not found");
        }

        entity.setSaved();
        return entity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T save(T entity) {
        validate(entity);

        T savedEntity;
        if (entity.isNew()) {
            savedEntity = dao.insert(entity);
        } else {
            savedEntity = dao.update(entity);
        }

        if (savedEntity == null) {
            throw new OperationFailedException("save failed failed");
        }

        savedEntity.setSaved();
        return savedEntity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteById(ID id) {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }
        if (!dao.deleteById(id)) {
            throw new OperationFailedException("delete failed");
        }
    }

}
