package com.katonaaron.order.bll.service;

import java.util.List;

/**
 * Represents a service for persisting an entity with the database.
 *
 * @param <T>  Type of the entity.
 * @param <ID> Type of the primary key of the entity.
 */
public interface Service<T, ID> {
    /**
     * Finds all entities from the database.
     *
     * @return list of found entities.
     * @throws com.katonaaron.order.exception.OperationFailedException if the entities could not be retrieved.
     */
    List<T> findAll();

    /**
     * Finds an entity by its id.
     *
     * @param id id of the entity.
     * @return found entity.
     * @throws IllegalArgumentException         if id is null.
     * @throws java.util.NoSuchElementException if the entity could not be found.
     */
    T findById(ID id);

    /**
     * Saves an entity in the database. If it is newly created insertion will take place, otherwise an update.
     *
     * @param t the entity to be saved.
     * @return the saved entity.
     * @throws com.katonaaron.order.exception.OperationFailedException  if the entity could not be saved.
     * @throws com.katonaaron.order.exception.EntityReflectionException if the entity does not correspond to the
     *                                                                  required format, resulting in an error, which occurred during a reflection operation.
     */
    T save(T t);

    /**
     * Deletes an entity from the database given by its id.
     *
     * @param id id of the entity.
     * @throws IllegalArgumentException                                if id is null.
     * @throws com.katonaaron.order.exception.OperationFailedException if deletion failed.
     */
    void deleteById(ID id);
}
