package com.katonaaron.order.bll.service;

import com.katonaaron.order.bll.validator.ClientAddressValidator;
import com.katonaaron.order.bll.validator.ClientNameValidator;
import com.katonaaron.order.dal.dao.DAO;
import com.katonaaron.order.model.Client;

import java.util.List;

/**
 * Represents a service for persisting a Client entity with the database.
 */
public class ClientService extends AbstractService<Client, String> {
    /**
     * Passes the validator and the DAO to the parent constructor {@link AbstractService#AbstractService(List, DAO)}.
     *
     * @param clientDAO the DAO which will be used for persistence.
     */
    public ClientService(DAO<Client, String> clientDAO) {
        super(List.of(new ClientNameValidator(), new ClientAddressValidator()), clientDAO);
    }
}
