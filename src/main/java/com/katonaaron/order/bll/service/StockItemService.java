package com.katonaaron.order.bll.service;

import com.katonaaron.order.dal.dao.DAO;
import com.katonaaron.order.model.StockItem;

import java.util.List;

/**
 * Represents a service for persisting a StockItem entity with the database.
 */
public class StockItemService extends AbstractService<StockItem, String> {
    /**
     * Passes the validator and the DAO to the parent constructor {@link AbstractService#AbstractService(List, DAO)}.
     *
     * @param stockItemDAO the DAO which will be used for persistence.
     */
    public StockItemService(DAO<StockItem, String> stockItemDAO) {
        super(List.of(), stockItemDAO); //TODO: add validators
    }
}
