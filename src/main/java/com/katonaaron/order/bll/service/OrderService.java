package com.katonaaron.order.bll.service;

import com.katonaaron.order.dal.dao.DAO;
import com.katonaaron.order.model.Order;

import java.util.List;

/**
 * Represents a service for persisting a Order entity with the database.
 */
public class OrderService extends AbstractService<Order, Integer> {
    /**
     * Passes the validator and the DAO to the parent constructor {@link AbstractService#AbstractService(List, DAO)}.
     *
     * @param dao the DAO which will be used for persistence.
     */
    public OrderService(DAO<Order, Integer> dao) {
        super(List.of(), dao);
    }
}
