package com.katonaaron.order.bll.service;

import com.katonaaron.order.dal.dao.DAO;
import com.katonaaron.order.model.Product;

import java.util.List;

/**
 * Represents a service for persisting a Product entity with the database.
 */
public class ProductService extends AbstractService<Product, String> {
    /**
     * Passes the validator and the DAO to the parent constructor {@link AbstractService#AbstractService(List, DAO)}.
     *
     * @param productDAO the DAO which will be used for persistence.
     */
    public ProductService(DAO<Product, String> productDAO) {
        super(List.of(), productDAO); //TODO: add validators
    }
}
