package com.katonaaron.order.exception;

/**
 * Exception thrown when an entity does not have a field marked by @Id.
 */
public class MissingIdException extends RuntimeException {
    public MissingIdException() {
    }

    public MissingIdException(String message) {
        super(message);
    }

    public MissingIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingIdException(Throwable cause) {
        super(cause);
    }
}
