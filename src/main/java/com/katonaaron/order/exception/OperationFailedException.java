package com.katonaaron.order.exception;

/**
 * Exception thrown when an operation failed.
 */
public class OperationFailedException extends RuntimeException {

    public OperationFailedException() {
    }

    public OperationFailedException(String message) {
        super(message);
    }

    public OperationFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public OperationFailedException(Throwable cause) {
        super(cause);
    }
}
