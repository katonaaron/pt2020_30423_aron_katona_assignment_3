package com.katonaaron.order.exception;

/**
 * Exception thrown when the pool reached its maximum capacity.
 */
public class MaxPoolSizeReachedException extends RuntimeException {
    public MaxPoolSizeReachedException() {
    }

    public MaxPoolSizeReachedException(String message) {
        super(message);
    }

    public MaxPoolSizeReachedException(String message, Throwable cause) {
        super(message, cause);
    }

    public MaxPoolSizeReachedException(Throwable cause) {
        super(cause);
    }
}
