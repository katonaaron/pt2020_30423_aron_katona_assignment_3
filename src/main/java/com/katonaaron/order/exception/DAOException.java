package com.katonaaron.order.exception;

/**
 * Exception thrown when an error occurred in the DAO.
 */
public class DAOException extends RuntimeException {
    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
