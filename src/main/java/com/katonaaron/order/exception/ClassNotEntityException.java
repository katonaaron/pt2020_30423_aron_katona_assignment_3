package com.katonaaron.order.exception;

/**
 * Exception thrown when a class does not have the @Entity annotation.
 */
public class ClassNotEntityException extends EntityReflectionException {
    public ClassNotEntityException() {
    }

    public ClassNotEntityException(String message) {
        super(message);
    }

    public ClassNotEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClassNotEntityException(Throwable cause) {
        super(cause);
    }
}
