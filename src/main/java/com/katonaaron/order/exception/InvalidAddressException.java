package com.katonaaron.order.exception;

/**
 * Exception thrown when an address is invalid.
 */
public class InvalidAddressException extends ValidationException {
    public InvalidAddressException() {
    }

    public InvalidAddressException(String message) {
        super(message);
    }

    public InvalidAddressException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidAddressException(Throwable cause) {
        super(cause);
    }
}
