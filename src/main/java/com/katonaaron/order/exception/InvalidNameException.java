package com.katonaaron.order.exception;

/**
 * Exception thrown when a name is invalid.
 */
public class InvalidNameException extends ValidationException {
    public InvalidNameException() {
    }

    public InvalidNameException(String message) {
        super(message);
    }

    public InvalidNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidNameException(Throwable cause) {
        super(cause);
    }
}
