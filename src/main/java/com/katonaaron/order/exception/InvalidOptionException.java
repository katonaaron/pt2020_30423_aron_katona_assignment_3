package com.katonaaron.order.exception;

/**
 * Exception thrown when an option is invalid.
 */
public class InvalidOptionException extends RuntimeException {
    public InvalidOptionException() {
    }

    public InvalidOptionException(String message) {
        super(message);
    }

    public InvalidOptionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidOptionException(Throwable cause) {
        super(cause);
    }
}
