package com.katonaaron.order.exception;

/**
 * Exception thrown when an error occurred during a reflection operation on an entity.
 */
public class EntityReflectionException extends RuntimeException {
    public EntityReflectionException() {
    }

    public EntityReflectionException(String message) {
        super(message);
    }

    public EntityReflectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityReflectionException(Throwable cause) {
        super(cause);
    }
}
