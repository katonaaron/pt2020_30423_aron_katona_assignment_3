package com.katonaaron.order.model;

import com.katonaaron.order.annotations.Column;
import com.katonaaron.order.annotations.Entity;
import com.katonaaron.order.annotations.Id;

import java.util.Objects;

/**
 * Represents a client of the warehouse.
 */
@Entity
public class Client extends AbstractEntity {
    /**
     * Name of the client.
     */
    @Id
    @Column
    private String name;

    /**
     * Address of the client
     */
    @Column
    private String address;

    /**
     * Create client with empty values.
     */
    public Client() {
        name = address = "";
    }

    /**
     * Regular constructor.
     *
     * @param name    name of the client.
     * @param address address of the client.
     */
    public Client(String name, String address) {
        this.name = name;
        this.address = address;
    }

    /**
     * Copy constructor.
     *
     * @param oldClient client to be duplicated.
     */
    public Client(Client oldClient) {
        this.name = oldClient.name;
        this.address = oldClient.address;
    }

    /**
     * Returns the field of the client marked with @Id
     *
     * @return the id of the client
     */
    public String getId() {
        return name;
    }

    /**
     * Gets the name of the client.
     *
     * @return name of the client.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the client.
     *
     * @param name name of the client.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the address of the client.
     *
     * @return address of the client.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address of the client.
     *
     * @param address address of the client
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Returns a string representation of the object
     *
     * @return the string representation.
     */
    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o the other object.
     * @return true if the object is equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return Objects.equals(getName(), client.getName()) &&
                Objects.equals(getAddress(), client.getAddress());
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
