package com.katonaaron.order.model;

import com.katonaaron.order.annotations.*;

import java.util.Objects;

/**
 * Represents a warehouse order.
 */
@Entity
@Table(name = "orders")
public class Order extends AbstractEntity {
    /**
     * Auto generated primary key of the order.
     */
    @Id
    @GeneratedValue
    @Column
    private Integer id;

    /**
     * Name of the client that created the order.
     */
    @Column
    private String clientName;

    /**
     * Name of the ordered product.
     */
    @Column
    private String productName;

    /**
     * The number of ordered products.
     */
    @Column
    private Integer quantity;

    /**
     * Empty constructor.
     */
    public Order() {
    }

    /**
     * Regular constructor.
     *
     * @param clientName  name of the client.
     * @param productName name of the prodcut.
     * @param quantity    quantity of the ordered product
     */
    public Order(String clientName, String productName, Integer quantity) {
        this.clientName = clientName;
        this.productName = productName;
        this.quantity = quantity;
    }

    /**
     * Returns the field of the order marked with @Id
     *
     * @return the id of the order
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id of the order
     *
     * @param id the id of the order
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name of the client that placed the order.
     *
     * @return name of the client.
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Sets the name of the client that placed the order.
     *
     * @param clientName name of the client.
     */
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * Gets the name of the ordered product.
     *
     * @return name of the product.
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the name of the ordered product.
     *
     * @param productName name of the product.
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Gets the quantity of the ordered product.
     *
     * @return quantity of product.
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Gets the quantity of the ordered product.
     *
     * @param quantity quantity of product.
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Returns a string representation of the object
     *
     * @return the string representation.
     */
    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", clientName='" + clientName + '\'' +
                ", productName='" + productName + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o the other object.
     * @return true if the object is equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return Objects.equals(getClientName(), order.getClientName()) &&
                Objects.equals(getProductName(), order.getProductName()) &&
                Objects.equals(getQuantity(), order.getQuantity());
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(getClientName(), getProductName());
    }
}
