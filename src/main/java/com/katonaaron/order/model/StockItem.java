package com.katonaaron.order.model;

import com.katonaaron.order.annotations.Column;
import com.katonaaron.order.annotations.Entity;
import com.katonaaron.order.annotations.Id;
import com.katonaaron.order.annotations.Table;

import java.util.Objects;

/**
 * Represents a warehouse item: a product and its quantity.
 */
@Entity
@Table(name = "stock")
public class StockItem extends AbstractEntity {
    /**
     * Name of the product.
     */
    @Id
    @Column
    private String productName;

    /**
     * Quantity of the product.
     */
    @Column
    private Integer quantity;

    /**
     * Empty constructor.
     */
    public StockItem() {
    }

    /**
     * Regular constructor: sets the field values.
     *
     * @param productName name of the product.
     * @param quantity    quantity of the product on stock.
     */
    public StockItem(String productName, Integer quantity) {
        this.productName = productName;
        this.quantity = quantity;
    }

    /**
     * Copy constructor.
     *
     * @param oldStockItem old stock item to be copied.
     */
    public StockItem(StockItem oldStockItem) {
        this.productName = oldStockItem.productName;
        this.quantity = oldStockItem.quantity;
    }

    /**
     * Returns the field of the product marked with @Id.
     *
     * @return the id of the product.
     */
    public String getId() {
        return productName;
    }

    /**
     * Gets the name of the product.
     *
     * @return name of the product.
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the name of the product.
     *
     * @param productName name of the product.
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Gets the number of products in the warehouse.
     *
     * @return quantity of products on stock.
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Sets the number of products in the warehouse.
     *
     * @param quantity quantity of products on stock.
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Increases the number of products in the warehouse by an increment.
     *
     * @param diff the increment.
     */
    public void increaseQuantity(int diff) {
        this.quantity += diff;
    }

    /**
     * Returns a string representation of the object
     *
     * @return the string representation.
     */
    @Override
    public String toString() {
        return "StockItem{" +
                "productName='" + productName + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o the other object.
     * @return true if the object is equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StockItem)) return false;
        StockItem stockItem = (StockItem) o;
        return Objects.equals(getProductName(), stockItem.getProductName()) &&
                Objects.equals(getQuantity(), stockItem.getQuantity());
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(getProductName());
    }
}
