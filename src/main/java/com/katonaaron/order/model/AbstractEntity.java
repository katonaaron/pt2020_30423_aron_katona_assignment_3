package com.katonaaron.order.model;

/**
 * Represents an entity that will be persisted with the database.
 */
public abstract class AbstractEntity {
    /**
     * Stores whether the entity is new, or has been inserted before to the database.
     */
    private boolean isNew = true;

    /**
     * Mark the entity as saved to the database.
     */
    public void setSaved() {
        isNew = false;
    }

    /**
     * Verifies whether the entity is new, or has been inserted before to the database.
     *
     * @return true if the entity is new
     */
    public boolean isNew() {
        return isNew;
    }
}
