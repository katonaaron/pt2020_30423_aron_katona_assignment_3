package com.katonaaron.order.model;

import com.katonaaron.order.annotations.Column;
import com.katonaaron.order.annotations.Entity;
import com.katonaaron.order.annotations.Id;

import java.util.Objects;

/**
 * Represents a warehouse product.
 */
@Entity
public class Product extends AbstractEntity {
    /**
     * Name of the product.
     */
    @Id
    @Column
    private String name;

    /**
     * Unit price of the product.
     */
    @Column
    private Double unitPrice;

    /**
     * Empty constructor.
     */
    public Product() {
    }

    /**
     * Regular constructor: sets the field values.
     *
     * @param name      name of the product.
     * @param unitPrice unit price of the product
     */
    public Product(String name, Double unitPrice) {
        this.name = name;
        this.unitPrice = unitPrice;
    }

    /**
     * Copy constructor.
     *
     * @param oldProduct old product ot be copied.
     */
    public Product(Product oldProduct) {
        this.name = oldProduct.name;
        this.unitPrice = oldProduct.unitPrice;
    }

    /**
     * Returns the field of the product marked with @Id.
     *
     * @return the id of the product.
     */
    public String getId() {
        return name;
    }

    /**
     * Gets the name of the product.
     *
     * @return name of the product.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the product.
     *
     * @param name name of the product.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the unit price of the product.
     *
     * @return unit price of the product.
     */
    public Double getUnitPrice() {
        return unitPrice;
    }

    /**
     * Sets the unit price of the product.
     *
     * @param unitPrice unit price of the product.
     */
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * Returns a string representation of the object
     *
     * @return the string representation.
     */
    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", unitPrice=" + unitPrice +
                '}';
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param o the other object.
     * @return true if the object is equal.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Objects.equals(getName(), product.getName()) &&
                Objects.equals(getUnitPrice(), product.getUnitPrice());
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
