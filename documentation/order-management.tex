 
\documentclass[a4paper,10pt]{article}
\usepackage{fontspec}
\usepackage[singlespacing]{setspace}
\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\setmainfont{Times New Roman}

%opening
\title{Order Management}
\date{April 17, 2020}
\author{Katona Áron}

\begin{document}

    \pagenumbering{gobble}
    \maketitle
    \newpage
    \pagenumbering{roman}
    \tableofcontents
    \newpage
    \pagenumbering{arabic}
    
    \section{Objective}
    The main objective is to design and implement an order management application aiming to process customer orders for a warehouse.
    \par
    Relational databases are used to achieve persistent data storage, and the commands are read from a text file given as a command line argument. Furthermore, the application is structured using a layered architecture. 
    
    \subsection{Secondary objectives}
    The main objective can be divided into multiple independent steps:
    \begin{itemize}
     \item \textbf{Reading the commands from a text file} 
      \hyperref[sec:input]{(\ref{sec:input})}
     \item \textbf{Input validation}
      \hyperref[sec:validation]{(\ref{sec:validation})}
     \item \textbf{Creating the appropriate models for storing the data}
      \hyperref[sec:model]{(\ref{sec:model})}
     \item \textbf{Realization of a generic Data Access Object}
      \hyperref[sec:dao]{(\ref{sec:dao})}
     \item \textbf{Connecting to the database}
      \hyperref[sec:des_conn_pool]{(\ref{sec:des_conn_pool})}
      \hyperref[sec:conn_pool]{(\ref{sec:conn_pool})}
     \item \textbf{Creating a layered architecture with increased abstractions}
      \hyperref[sec:packages]{(\ref{sec:packages})}
     \item \textbf{Generating reports into PDF files}
      \hyperref[sec:output]{(\ref{sec:output})}
    \end{itemize}

    
    \section{Problem analysis and use cases}
    \subsection{Problem analysis}
    \label{sec:analysis}
    The main steps of the problem:
    \begin{enumerate}
     \item \label{item:a1} Read the commands from a text file.
     \item \label{item:a2} Create the corresponding model objects.
     \item \label{item:a3} Communicate with the database.
     \item \label{item:a4} Write the results.
    \end{enumerate}
    \ref{item:a1}: A file parsing methodology should be created in order to interpret the commands.\\
    \ref{item:a2}: The entities that are subject of the commands, should be appropriately modeled.\\
    \ref{item:a3}: A connection should be made to the database, and classes should be written to encapsulate the queries such that only entity operations should be visible.\\
    4: A PDF generator should be created.
    
    \subsection{Input format}
    \label{sec:input_format}
    The input file consists of lines of commands, having the following format:
    \begin{verbatim}
     Insert|Delete|Report client|product|order[: parameters...]
    \end{verbatim}
    And the following command:
    \begin{verbatim}
     Order: parameters...
    \end{verbatim}
    Is an alias of:
    \begin{verbatim}
     Insert order: parameters...
    \end{verbatim}
    An the \verb|parameters...| option consists of line separated fields of the entity given as the second argument.
    Example:
    \begin{verbatim}
     Insert client: Ion Popescu, Bucuresti
     Insert product: orange, 40, 1.5
     Order: Ion Popescu, orange, 5
     Report order
    \end{verbatim}
    \paragraph{Note} 
    \begin{enumerate}
     \item The clients and the products are identified by their names.
     \item If multiple \verb|Insert product| commands are given with the same product name but different unit prices, the unit price of the product will be the initial one.
     \item After each \verb|Order| command a new pdf file is created, representing a different order.
    \end{enumerate}

    
    \subsection{Output format}
    After each \verb|Report| or \verb|Order| command, a new PDF file is created having the following format:
    \begin{verbatim}
     report-<entity>-<nr>.pdf
    \end{verbatim}
    Where \verb|<entity>| can be one of the entities (\verb|client|, \verb|product|, \verb|order|), and \verb|<nr>| is the number of the pdf from the given category.
    
    \subsection{Use cases}
    \textbf{Use Case:} Execute a list of commands\\
    \textbf{Primary Actor:} User\\
    \textbf{Main Success Scenario:}
    \begin{enumerate}
        \item The user navigates to the location of the jar file.
        \item The user executes the \verb|dump.sql| file in order to create the database structure.
        \item \label{step:1} The user creates an input file with commands next to the jar, and fills it with values in the format described in (\ref{sec:input_format}).
        \item The user calls the program in the terminal using the following command:
        \begin{verbatim}java -jar PT2020_30423_Aron_Katona_Assignment_3.jar \
        <input_file>\end{verbatim}
        Where the \verb|<input_file>| corresponds to the name of the file created in step \ref{step:1}.
        \item The program generates a pdf file for each \verb|report| and \verb|order| command and then exits without any error message.
    \end{enumerate}

    \textbf{Alternative Sequences:}
    \begin{enumerate}
        \item File IO errors
        \begin{itemize}
            \item Occurs when a file could not be opened, written into or read from.
            \item The program exits with an error message printed to the terminal (STDERR).
        \end{itemize}
        \item The format or the content of the input file is invalid
        \begin{itemize}
            \item The program exits with an error message printed to the terminal (STDERR).
        \end{itemize}
    \end{enumerate}
    
    \section{Design}
    \subsection{Packages}
    \label{sec:packages}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=\linewidth]{figures/package_diagram.pdf}
        \caption{Package diagram} 
        \label{fig:package}
    \end{figure*}
    The application follows a layered architecture: 
    \begin{itemize}
     \item the \textbf{Data Access Layer} (\verb|dal| package) contains the classes for connecting to the database and executing queries.
     \item the \textbf{Business Logic Layer} (\verb|bll| package) contains the classes for working with entities, including validations.
     \item the \textbf{Presentation Layer} (\verb|presentation| package) contains the classes for external communication: reading the input file, controlling user requests and creating reports and PDF files.
    \end{itemize}
    Moreover there are some packages that are used in more than one layer:
    \begin{itemize}
     \item \verb|model| package contains the models of the database entities.
     \item \verb|command| package contains classes which have the responsibility of representing and executing commands.
     \item \verb|annotations| package contains custom annotations for entity persistence.
     \item \verb|exception| package contains the exceptions that are thrown by the classes.
     \item \verb|util| package contains classes that help in the reflection operations, mainly in the DAO classes.
    \end{itemize}
    On figure \ref{fig:package} one can see the package diagram described above.
    
    \subsection{Dependency Injection}
    \label{sec:dep_inj}
    Some classes such as \verb|DAO|s and \verb|Service|s can be instantiated only once, because they communicate with the database, thus each of their instances should give the same results for the same inputs.
    \par
    In order to allow for all dependent classes to have the same instance of a given class, dependency injection \cite{dep_inj} was used, more specifically, \textbf{constructor injection}. For each classes that depend on another class, the object is given as a constructor argument instead of using the \verb|new| keyword.
    \par
    This technique comes with great advantages:
    \begin{itemize}
     \item The dependent classes can be easily configurable. The implementation of the dependency can be easily changed, because the constructors require an interface.
     \item Decreases the coupling between a class and its dependency.
     \item The \textbf{D} (Dependency inversion principle) in \textbf{SOLID} is fulfilled: these classes depend upon abstractions, interfaces, instead of the concretions.
     \item Testing becomes easier, because one can create mock objects that implement the interface on which an object depends.
    \end{itemize}
    \par
    Because of the advantages shown above, dependency injection was used instead of singleton classes or composition.
    \par
    The application starts from the \verb|App| class. This class handles the dependency injection, by instantiating each object in the order of dependency.
    \par
    The usage of annotations for data persistence and the dependency injection was inspired from the Hibernate and Spring frameworks.
    
    \subsection{Connection pool}
    \label{sec:des_conn_pool}
    If a new database connection is created for each query, a significant overhead is followed. But a single connection for the whole application could cause additional problems in a multi-threaded environment. Thus a \textbf{connection pool} was created. It is described in (\ref{sec:conn_pool}).
    
    \section{Implementation}    
    \subsection{Annotations}
    Custom annotations \cite{annotation} were used on fields and classes for data persistence.
    \par
    \verb|@Entity| represents a database entity, that should be mapped. Using the \verb|@Table| annotation one can specify the name of the table, otherwise the name of the class is used. These annotations are placed on classes.
    \par
    For the fields one can use the \verb|@Column| annotation, which specifies that it should be mapped with a database column. The \verb|@Id| annotates the primary key of the entity and the \verb|@GeneratedValue| shows that the field is generated in the database and should be fetched its value after insertion.
    \par 
    Other annotations were created in order to support relationships, but in the end they were not used.
    
    \subsection{Model}
    \label{sec:model}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=\linewidth]{figures/ER.pdf}
        \caption{ER-diagram of the database} 
        \label{fig:ER}
    \end{figure*}
    The models are objects that map the tables of the databases.
    \par 
    Each entity extends the \verb|AbstractEntity|, which has the methods for checking whether the object was saved in the database or not. The \verb|Client| entity represents a client, having as key its name and as an attribute its address. The \verb|Product| entity contains the name and unit price of a product. 
    \par
    The number of items of each product in the warehouse is stored in a different table/entity, the \verb|stock| table and in the \verb|StockItem| entity. This is done for keeping the product in the database when it's out of stock.
    \par 
    The \verb|Order| entity contains the name of the product and the client, and the quantity of the ordered product.
    \par
    On figure \ref{fig:ER} one can see the ER-diagram of the database.
    
    \subsection{Command}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.8\linewidth]{figures/command.pdf}
        \caption{command package} 
        \label{fig:command}
    \end{figure*}
    A \verb|Command| is made of a \verb|CommandType| enum (INSERT, DELETE, REPORT), an entity on which the command is applied (client, product, order), and additional parameters such as fields or keys. The format is described in (\ref{sec:input_format}).
    \par
    The \verb|CommandManager| has the responsibility of receiving and executing commands.
    
    \subsection{Data Access Layer}
    \subsubsection{Connection Pool}
    \label{sec:conn_pool}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.6\linewidth]{figures/connection.pdf}
        \caption{connection package} 
        \label{fig:connection}
    \end{figure*}
    As described in (\ref{sec:des_conn_pool}), the connection pool \cite{connection_pool} has an important role. To allow different kinds of implementations, the \verb|ConnectionPool| interface is used in the dependent classes, which is implemented by the \verb|BasicConnectionPool| class.
    \par
    This singleton class in its factory method creates a list of database  connections of size \verb|INITIAL_POOL_SIZE|. When a new connection is requested, it takes one from the pool, or creates a new one until \verb|MAX_POOL_SIZE| connections are reached. When a connection was taken, it is put in the \verb|usedConnections| list. When it is released, the connection is put back to the \verb|connectionPool|, thus it can be reused. It also has a \verb|shutdown()| method for closing all connections.
    
    \subsubsection{Data Access Objects (DAO)}
    \label{sec:dao}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=\linewidth]{figures/dao.pdf}
        \caption{dao package} 
        \label{fig:dao}
    \end{figure*}
    These classes executes the INSERT, DELETE, SELECT and UPDATE SQL statements, representing the first abstraction layer between the database and the presentation layer. 
    \par
    The \verb|DAO| interface is implemented by the \verb|AbstractDAO| class. This class using reflection techniques, the model annotations and the utility classes, provides a generic implementation of a repository for any entity. The implemented methods are:
    \begin{itemize}
      \item \verb|insert|
      \item \verb|update|
      \item \verb|deleteById|
      \item \verb|findById|
      \item \verb|findAll|
    \end{itemize}
    \par
    The other DAO classes are specific for the entity, and they could extend/modify the behavior of the \verb|AbstractDAO| for that entity.
    
    \subsection{Business Logic Layer}
    \subsubsection{Services}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=\linewidth]{figures/service.pdf}
        \caption{service package} 
        \label{fig:service}
    \end{figure*}
    Services are the next step of abstraction. They have a common \verb|T save(T t);| method for inserting or updating an entity. The operation is differentiated by the methods of the \verb|AbstractEntity|. The services also provide input validation.
    \par
    Similarly to the DAOs, there is a \verb|Service| interface, an \verb|AbstractService| generic implementation and specific subclasses for each entity.
    
    \subsubsection{Validation}
    \label{sec:validation}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.7\linewidth]{figures/validator.pdf}
        \caption{validator package} 
        \label{fig:validator}
    \end{figure*}
    The validators are classes that implement the \verb|void validate(T t);| method of the \verb|Validator| interface. Instances of this interface are passed to the services, where they use them to validate the entities before saving them.
    
    \subsection{Presentation Layer}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.8\linewidth]{figures/presentation_layer.pdf}
        \caption{Presentation layer} 
        \label{fig:presentaion_layer}
    \end{figure*}
    \subsubsection{Input - Reading commands from file}
    \label{sec:input}
    For reading commands from file the \verb|io| and \verb|transformer| packages were used, which are similar to the ones from the queue-simulator project. Using the iterator pattern the \verb|ObjectReader| reads objects (in our case the commands) from a file in sequential order, creating objects from each line by using the \verb|Transformer| interface and the \verb|CommandTransformer| implementation. 
    
    \subsubsection{Output - Creating PDF files}
    \label{sec:output}
    For creating reports with tables the \verb|report| package is used. The \verb|ReportGenerator| generic class creates a PDF containing a table having as columns the \verb|@Column| fields of the entities, and as rows the objects from the list given in the parameter of the method \verb|void generateReport(List<T> entities)|. For specific entities, specific report generators were created.
    \par
    To write a single text into a PDF, the \verb|io.PDFWriter| class is used.
    
    \subsubsection{Controllers}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=\linewidth]{figures/controller.pdf}
        \caption{controller package} 
        \label{fig:controller}
    \end{figure*}
    The controllers execute the commands by using the repositories and the report classes.
    \par
    For each command type, a different controller is created, all implementing the \verb|CommandController| interface. The \verb|InsertController| handles the insertion commands, including the order creation, and the checking of the product quantity. The \verb|DeleteController| provides the deletion of the entities, and the \verb|ReportController| generates reports of each entity.
    
    \subsection{Utility classes}
    \begin{figure*}[!htbp]
        \centering
        \includegraphics[width=0.5\linewidth]{figures/util.pdf}
        \caption{util package} 
        \label{fig:util}
    \end{figure*}
    The \verb|ClassUtil| has an only method for checking whether a class is of wrapper type.
    \par
    The \verb|EntityUtil| class using the annotations placed on the models, provides utility methods for:
    \begin{itemize}
     \item Instantiating an entity from a list of field values.
     \item Getting the primary key (\verb|@Id|) field of an entity.
     \item Getting the name of a column or a table.
     \item Getting a list of columns or generated columns.
    \end{itemize}

    \section{Result}
    Because of using dependency injections, the classes can be individually tested by providing mock objects for them. Thus by using frameworks like JUnit and Mockito one can effectively test the behavior of each class.
    \par
    The application was verified by providing inputs of edge cases and observing the behavior and the results.
    
    \section{Conclusion}
    The main objective was achieved by completing the secondary objectives.
    \par
    A generic implementation was given for reading objects from a text file and writing a list of entities into a table inside a PDF.
    \par
    Model persistence was implemented by using annotations \cite{annotation} and a generic DAO. The connection to the database was implemented using a connection pool 
    \cite{connection_pool}  in order to resolve the issues presented in (\ref{sec:des_conn_pool}).
    \par
    The data needed for database connections is read from a properties resource file \cite{properties}. This avoids the hard-coding of the data and the ease of configuration.
    \par
    The usage of a layered architecture emphasized the importance of abstraction: one does not have to make database queries for each entity operations, instead an abstraction was given in the Data Access Layer, A further abstraction in the Business Logic Layer, resulting in an easier Presentation Layer.
    \par
    The usage of dependency injection \cite{dep_inj} proved to be an important part of application development, because of the provided advantages presented in (\ref{sec:dep_inj}).
    \par
    Design patterns were needed to achieve the main objective:
    \begin{itemize}
     \item Dependency injection.
     \item Singleton pattern for the database connection pool.
     \item Iterator pattern for sequential reading of commands from the input file.
    \end{itemize}
    \par
    The application can be further developed by:
    \begin{itemize}
     \item Providing a mechanism for relationships (One-One, One-Many,...) in the DAO.
     \item Providing a graphical user interface.
    \end{itemize}

    
    \bibliography{bibliography}
    \bibliographystyle{plain}
    
\end{document}
